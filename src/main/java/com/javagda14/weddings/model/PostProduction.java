package com.javagda14.weddings.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PostProduction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String remainingPayment;
    private Boolean isPaid;
    private String transportCosts;
    private Boolean firstOperatorPaid;
    private Boolean secondOperatorPaid;
    private String clientNotes;
    private String editorsResponse;
    private String longFilmLink;
    private String shortFilmLink;
    private Boolean accepted;
    private Boolean pendriveSent;
    @OneToOne
    private ClientInfo clientInfo;
}
