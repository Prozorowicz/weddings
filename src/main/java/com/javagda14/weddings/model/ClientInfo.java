package com.javagda14.weddings.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDate weddingDate;
    private String brideName;
    private String brideSurname;
    private String groomName;
    private String groomSurname;
    private String idNumber;
    private String address;
    private String bridePhone;
    private String groomPhone;
    private String email;
    private Boolean contract;
    private Boolean advertising;
    private Boolean advance;
    private Boolean drone;
    @ManyToOne
    private Offer offer;
    @OneToOne(mappedBy = "clientInfo", cascade = CascadeType.REMOVE)
    private WeddingInfo weddingInfo;
    @OneToOne(mappedBy = "clientInfo", cascade = CascadeType.REMOVE)
    private PostProduction postProduction;



}
