package com.javagda14.weddings.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WeddingInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String price;
    private String bridePrep;
    private String groomPrep;
    private String ceremonyPlace;
    private String weddingPlace;
    private String distance;
    private String distancePayment;
    @ManyToOne
    private AppUser firstOperator;
    @ManyToOne
    private AppUser secondOperator;
    @OneToOne
    private ClientInfo clientInfo;
    /*if (pakiet=inny) cena,
            else: cena=pakiet+1300(if dron true)
    data wesela
    miejsce przygotowań panny młodej
    miejsce przygotowań pana młodego
    miejsce ślubu
    miejsce wesela
    odległość (jeśli ponad 100 km)
    dopłata za dojazd(odległość*1.5)
    główny operator(użytkownik o typie operator)
    drugi operator(użytkownik o typie operator)*/
}
