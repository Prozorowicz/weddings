package com.javagda14.weddings.model.dto;

import com.javagda14.weddings.model.PostProduction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterPostProductionDto {
    private String remainingPayment;
    private Boolean isPaid;
    private String transportCosts;
    private Boolean firstOperatorPaid;
    private Boolean secondOperatorPaid;
    private String clientNotes;
    private String editorsResponse;
    private String longFilmLink;
    private String shortFilmLink;
    private Boolean accepted;
    private Boolean pendriveSent;
    private Long clientInfoId;
    private Long modifiedId;

    public static RegisterPostProductionDto createFrom(PostProduction postProduction) {
        return RegisterPostProductionDto
                .builder()
                .remainingPayment(postProduction.getRemainingPayment())
                .isPaid(postProduction.getIsPaid())
                .transportCosts(postProduction.getTransportCosts())
                .firstOperatorPaid(postProduction.getFirstOperatorPaid())
                .secondOperatorPaid(postProduction.getSecondOperatorPaid())
                .clientNotes(postProduction.getClientNotes())
                .editorsResponse(postProduction.getEditorsResponse())
                .longFilmLink(postProduction.getLongFilmLink())
                .shortFilmLink(postProduction.getShortFilmLink())
                .accepted(postProduction.getAccepted())
                .pendriveSent(postProduction.getPendriveSent())
                .clientInfoId(postProduction.getClientInfo().getId())
                .build();

    }
}
