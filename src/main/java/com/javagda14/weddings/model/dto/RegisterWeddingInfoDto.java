package com.javagda14.weddings.model.dto;

import com.javagda14.weddings.model.WeddingInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterWeddingInfoDto {
    private String price;
    private String bridePrep;
    private String groomPrep;
    private String ceremonyPlace;
    private String weddingPlace;
    private String distance;
    private String distancePayment;
    private Long firstOperatorID;
    private Long secondOperatorID;
    private Long clientInfoId;
    private Long modifiedId;

    public static RegisterWeddingInfoDto createFrom(WeddingInfo weddingInfo) {
        return RegisterWeddingInfoDto
                .builder()
                .price(weddingInfo.getPrice())
                .bridePrep(weddingInfo.getBridePrep())
                .groomPrep(weddingInfo.getGroomPrep())
                .ceremonyPlace(weddingInfo.getCeremonyPlace())
                .weddingPlace(weddingInfo.getWeddingPlace())
                .distance(weddingInfo.getDistance())
                .distancePayment(weddingInfo.getDistancePayment())
                .firstOperatorID(weddingInfo.getFirstOperator().getId())
                .secondOperatorID(weddingInfo.getSecondOperator().getId())
                .clientInfoId(weddingInfo.getClientInfo().getId())
                .build();
    }
}
