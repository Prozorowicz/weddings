package com.javagda14.weddings.model.dto;

import com.javagda14.weddings.model.ClientInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterClientInfoDto {
    private String weddingDate;
    private String brideName;
    private String brideSurname;
    private String groomName;
    private String groomSurname;
    private String idNumber;
    private String address;
    private String bridePhone;
    private String groomPhone;
    private String email;
    private Boolean contract;
    private Boolean advertising;
    private Boolean advance;
    private Boolean drone;
    private Long offerID;

    public static RegisterClientInfoDto createFrom(ClientInfo clientInfo) {
        return RegisterClientInfoDto
                .builder()
                .weddingDate(clientInfo.getWeddingDate().toString())
                .brideName(clientInfo.getBrideName())
                .brideSurname(clientInfo.getBrideSurname())
                .groomName(clientInfo.getGroomName())
                .groomSurname(clientInfo.getGroomSurname())
                .idNumber(clientInfo.getIdNumber())
                .address(clientInfo.getAddress())
                .bridePhone(clientInfo.getBridePhone())
                .groomPhone(clientInfo.getGroomPhone())
                .email(clientInfo.getEmail())
                .contract(clientInfo.getContract())
                .advertising(clientInfo.getAdvertising())
                .advance(clientInfo.getAdvance())
                .drone(clientInfo.getDrone())
                .offerID(clientInfo.getOffer().getId())
                .build();
    }
}
