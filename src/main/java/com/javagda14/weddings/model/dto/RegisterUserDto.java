package com.javagda14.weddings.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.javagda14.weddings.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserDto {
    @Email
    @NotEmpty
    private String email;
    @NotEmpty
    private String username;
    @NotNull
    @Size(min = 3, max = 255)
    private String name;

    @NotEmpty
    @Size(min = 3, max = 255)
    private String surname;
    @JsonIgnore
    @NotEmpty
    @Size(min = 5, max = 255)
    private String password;
}
