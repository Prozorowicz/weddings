package com.javagda14.weddings.controller;

import com.javagda14.weddings.model.ClientInfo;
import com.javagda14.weddings.model.PostProduction;
import com.javagda14.weddings.model.dto.RegisterPostProductionDto;
import com.javagda14.weddings.service.ClientInfoService;
import com.javagda14.weddings.service.PostProductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@RequestMapping("/")
@Controller
public class PostProductionController {
    @Autowired
    private PostProductionService postProductionService;
    @Autowired
    private ClientInfoService clientInfoService;


    @GetMapping("/addPostProduction/{clientInfoId}")
    public String getAddView(Model model, @PathVariable(name = "clientInfoId") Long clientId) {
        Optional<ClientInfo> optionalClientInfo = clientInfoService.findById(clientId);
        if (optionalClientInfo.isPresent()) {
            RegisterPostProductionDto registerPostProductionDto = new RegisterPostProductionDto();
            registerPostProductionDto.setClientInfoId(clientId);
            model.addAttribute("added_postProduction", registerPostProductionDto);
            model.addAttribute("clientInfo",optionalClientInfo.get());
            return "wedding/postProductionForm";
        }
        return "redirect:/";
    }

    @PostMapping("/addPostProduction")
    public String getAddView(Model model, RegisterPostProductionDto registerPostProductionDto) {
        Optional<ClientInfo> clientInfoOptional = clientInfoService.findById(registerPostProductionDto.getClientInfoId());
        if (clientInfoOptional.isPresent()) {
            if (registerPostProductionDto.getRemainingPayment().isEmpty() ) {
                model.addAttribute("added_postProduction", registerPostProductionDto);
                model.addAttribute("clientInfo",clientInfoOptional.get());
                model.addAttribute("error_message", "Need remaining payment filled");
                return "wedding/postProductionForm";
            }

            Optional<PostProduction> postProductionOptional = postProductionService.register(registerPostProductionDto);
            if (!postProductionOptional.isPresent()) {
                model.addAttribute("added_postProduction", registerPostProductionDto);
                model.addAttribute("clientInfo",clientInfoOptional.get());
                model.addAttribute("error_message", "something went real bad :(");
                return "wedding/postProductionForm";
            }
            return "redirect:/list/";
        }

        return "redirect:/";
    }
    @GetMapping("/modifyPostProduction/{id}")
    public String modify(Model model, @PathVariable(name = "id", required = true) Long id) {
        Optional<ClientInfo> clientInfoOptional = clientInfoService.findById(id);
        if (clientInfoOptional.isPresent()) {
            Optional<PostProduction> postProductionOptional = postProductionService.findByClientInfoId(id);
            PostProduction postProduction = postProductionOptional.get();

            RegisterPostProductionDto dto = RegisterPostProductionDto.createFrom(postProduction);
            dto.setModifiedId(id);
            model.addAttribute("added_postProduction", dto );
            model.addAttribute("clientInfo",clientInfoOptional.get());

            return "wedding/PostProductionForm";
        }

        return "redirect:/";


    }

    @PostMapping("/modifyPostProduction")
    public String modify(RegisterPostProductionDto registerPostProductionDto) {
      Optional<PostProduction> postProductionOptional = postProductionService.modify( registerPostProductionDto);

       return "redirect:/list/";
    }
}

