package com.javagda14.weddings.controller;

import com.javagda14.weddings.model.AppUser;
import com.javagda14.weddings.model.ClientInfo;
import com.javagda14.weddings.model.WeddingInfo;
import com.javagda14.weddings.model.dto.RegisterClientInfoDto;
import com.javagda14.weddings.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequestMapping("/")
@Controller
public class ClientInfoActionsController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private ClientInfoService clientInfoService;
    @Autowired
    private OfferService offerService;
    @Autowired
    private WeddingInfoService weddingInfoService;
    @Autowired
    private PostProductionService postProductionService;

    @GetMapping("/list")
    public String getList(Model model, @RequestParam(name = "sort", required = false) String sort) {

        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();

        if (optionalAppUser.isPresent()) {

            List<ClientInfo> weddingsList =
                    clientInfoService.getAll();

            boolean sortLow = sort == null || sort.isEmpty() || sort.equals("0");

            Collections.sort(weddingsList, new Comparator<ClientInfo>() {
                @Override
                public int compare(ClientInfo o1, ClientInfo o2) {
                    if (o1.getWeddingDate().isAfter(o2.getWeddingDate())) {
                        return 1 * (sortLow ? -1 : 1);
                    } else if (o2.getWeddingDate().isAfter(o1.getWeddingDate())) {
                        return -1 * (sortLow ? -1 : 1);
                    }
                    return 0;
                }
            });

            model.addAttribute("weddingsList", weddingsList);
            model.addAttribute("otherSort", sortLow ? "1" : "0");

            return "wedding/list";
        }

        return "redirect:/appUser/login";
    }

    @GetMapping("/modifyClientInfo/{id}")
    public String modify(Model model, @PathVariable(name = "id", required = true) Long id) {
        Optional<ClientInfo> clientInfoOptional = clientInfoService.findById(id);
        if (clientInfoOptional.isPresent()) {
            ClientInfo clientInfo = clientInfoOptional.get();

            model.addAttribute("modify_clientInfo", RegisterClientInfoDto.createFrom(clientInfo));
            model.addAttribute("added_offer", offerService.findAll());
            model.addAttribute("id", id);
            /*model.addAttribute("added_offer", clientInfo.getOffer());*/

        } else {
            return "redirect:/";
        }

        return "wedding/clientInfoModify";
    }

    @PostMapping("/modifyClientInfo/{id}")
    public String modify(RegisterClientInfoDto registerClientInfoDto, @PathVariable(name = "id", required = true) Long id) {
        Optional<ClientInfo> clientInfoOptional = clientInfoService.modify(id, registerClientInfoDto);

        return "redirect:/modifyWeddingInfo/" + id;
    }

    @GetMapping("/removeWedding/{id}")
    public String removeWedding(@PathVariable(name = "id") Long id) {
        boolean result = clientInfoService.remove(id);

        if (result) {
            return "redirect:/list?message=Wedding has been removed.";
        }
        return "redirect:/list?error_message=Unable to remove this wedding!";
    }


}
