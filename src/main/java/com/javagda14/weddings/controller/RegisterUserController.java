package com.javagda14.weddings.controller;

import com.javagda14.weddings.model.AppUser;
import com.javagda14.weddings.model.ClientInfo;
import com.javagda14.weddings.model.WeddingInfo;
import com.javagda14.weddings.model.dto.RegisterUserDto;
import com.javagda14.weddings.repository.UserRoleRepository;
import com.javagda14.weddings.service.AppUserService;
import com.javagda14.weddings.service.ClientInfoService;
import com.javagda14.weddings.service.LoginService;
import com.javagda14.weddings.service.WeddingInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@RequestMapping("/")
@Controller
public class RegisterUserController {
    @Autowired
    private AppUserService appUserService;

    @GetMapping("/register")
    public String getAddView(Model model) {

        model.addAttribute("added_user", new RegisterUserDto());
        return "admin/registerForm";
    }

    @PostMapping("/register")
    public String getAddView(Model model, RegisterUserDto registerUserDto) {

        if (registerUserDto.getPassword().isEmpty() || registerUserDto.getPassword().length() < 5) {
            model.addAttribute("added_user", registerUserDto);
            model.addAttribute("error_message", "Too simple password.");
            return "admin/registerForm";
        }

        Optional<AppUser> appUserOptional = appUserService.register(registerUserDto);
        if (!appUserOptional.isPresent()) {
            model.addAttribute("added_user", registerUserDto);
            model.addAttribute("error_message", "User with that email already exists.");
            return "admin/registerForm";
        }
        return "redirect:/";
    }

}
