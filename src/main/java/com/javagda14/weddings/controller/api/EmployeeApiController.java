package com.javagda14.weddings.controller.api;

import com.javagda14.weddings.model.AppUser;
import com.javagda14.weddings.model.dto.RegisterUserDto;
import com.javagda14.weddings.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/*@RestController
@CrossOrigin
@RequestMapping("/api/employees")
public class EmployeeApiController {
    @Autowired
    private AppUserService appUserService;
    @GetMapping("/")
    public ResponseEntity<List<RegisterUserDto>> getEmployees(){
        return ResponseEntity.ok(appUserService.findAll()
        .stream()
        .map(appUser -> new RegisterUserDto(
                appUser.getEmail(),
                appUser.getUsername(),
                appUser.getName(),
                appUser.getSurname(),
                appUser.getPassword()))
                .collect(Collectors.toList()));
    }
    @PostMapping("/add")
    public ResponseEntity<AppUser> addAppUser(@RequestBody RegisterUserDto registerUserDto){
        appUserService.register(registerUserDto);
return ResponseEntity.ok().build();
    }
}*/
