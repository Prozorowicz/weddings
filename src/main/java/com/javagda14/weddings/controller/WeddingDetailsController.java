package com.javagda14.weddings.controller;

import com.javagda14.weddings.model.AppUser;
import com.javagda14.weddings.model.ClientInfo;
import com.javagda14.weddings.model.PostProduction;
import com.javagda14.weddings.model.WeddingInfo;
import com.javagda14.weddings.service.ClientInfoService;
import com.javagda14.weddings.service.LoginService;
import com.javagda14.weddings.service.PostProductionService;
import com.javagda14.weddings.service.WeddingInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;
@RequestMapping("/")
@Controller
public class WeddingDetailsController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private ClientInfoService clientInfoService;
    @Autowired
    private WeddingInfoService weddingInfoService;
    @Autowired
    private PostProductionService postProductionService;
    @GetMapping("/details/{id}")
    public String getDetails(Model model, @PathVariable(name = "id", required = true) Long id) {

        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        Optional<ClientInfo> clientInfoOptional =clientInfoService.findById(id);
        Optional<WeddingInfo> weddingInfoOptional=weddingInfoService.findByClientInfoId(id);
        Optional<PostProduction> postProductionOptional=postProductionService.findByClientInfoId(id);
        if (optionalAppUser.isPresent()) {

            model.addAttribute("ClientDetails", clientInfoOptional.get());
            model.addAttribute("WeddingDetails", weddingInfoOptional.get());
            model.addAttribute("PostProdDetails", postProductionOptional.get());


            return "wedding/details";
        }

        return "redirect:/appUser/login";
    }
}
