package com.javagda14.weddings.controller;

import com.javagda14.weddings.model.ClientInfo;
import com.javagda14.weddings.model.dto.RegisterClientInfoDto;
import com.javagda14.weddings.service.ClientInfoService;
import com.javagda14.weddings.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@RequestMapping("/")
@Controller
public class RegisterClientInfoController {
    @Autowired
    private OfferService offerService;
    @Autowired
    private ClientInfoService clientInfoService;

    @GetMapping("/addClientInfo")
    public String getAddView(Model model) {
        model.addAttribute("added_offer", offerService.findAll());
        model.addAttribute("added_clientInfo", new RegisterClientInfoDto());
        return "wedding/clientInfoForm";
    }

    @PostMapping("/addClientInfo")
    public String getAddView(Model model, RegisterClientInfoDto registerClientInfoDto) {

        if (registerClientInfoDto.getWeddingDate().isEmpty() || registerClientInfoDto.getEmail().isEmpty()) {
            model.addAttribute("added_clientInfo", registerClientInfoDto);
            model.addAttribute("added_offer", offerService.findAll());
            model.addAttribute("error_message", "Need date of wedding and a customer email");
            return "wedding/clientInfoForm";
        }

        Optional<ClientInfo> clientInfoOptional = clientInfoService.register(registerClientInfoDto);
        if (!clientInfoOptional.isPresent()) {
            model.addAttribute("added_clientInfo", registerClientInfoDto);
            model.addAttribute("added_offer", offerService.findAll());
            model.addAttribute("error_message", "something went real bad :(");
            return "wedding/clientInfoForm";
        }


        return "redirect:/addWeddingInfo/" + clientInfoOptional.get().getId();
    }
}
