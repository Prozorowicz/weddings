package com.javagda14.weddings.controller;

import com.javagda14.weddings.service.AppUserService;
import com.javagda14.weddings.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class LoginController {
    @Autowired
    private AppUserService appUserService;
    @Autowired
    private LoginService loginService;
    @GetMapping("/login")
    public String getLoginView() {
        return "appUser/loginForm";
    }
    @GetMapping("/")
    public String getIndex() {
        return "index";
    }
}
