package com.javagda14.weddings.controller;

import com.javagda14.weddings.model.ClientInfo;
import com.javagda14.weddings.model.WeddingInfo;
import com.javagda14.weddings.model.dto.RegisterWeddingInfoDto;
import com.javagda14.weddings.service.AppUserService;
import com.javagda14.weddings.service.ClientInfoService;
import com.javagda14.weddings.service.WeddingInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@RequestMapping("/")
@Controller
public class RegisterWeddingInfoController {
    @Autowired
    private AppUserService appUserService;
    @Autowired
    private WeddingInfoService weddingInfoService;
    @Autowired
    private ClientInfoService clientInfoService;


    @GetMapping("/addWeddingInfo/{clientInfoId}")
    public String getAddView(Model model, @PathVariable(name = "clientInfoId") Long clientId) {
        Optional<ClientInfo> optionalClientInfo = clientInfoService.findById(clientId);
        if (optionalClientInfo.isPresent()) {
            RegisterWeddingInfoDto registerWeddingInfoDto = new RegisterWeddingInfoDto();
            registerWeddingInfoDto.setClientInfoId(clientId);
            model.addAttribute("added_operator", appUserService.findAll());
            model.addAttribute("added_weddingInfo", registerWeddingInfoDto);
            return "wedding/weddingInfoForm";
        }
        return "redirect:/";
    }

    @PostMapping("/addWeddingInfo")
    public String getAddView(Model model, RegisterWeddingInfoDto registerWeddingInfoDto) {
        Optional<ClientInfo> clientInfoOptional = clientInfoService.findById(registerWeddingInfoDto.getClientInfoId());
        if (clientInfoOptional.isPresent()) {
            if (registerWeddingInfoDto.getCeremonyPlace().isEmpty() || registerWeddingInfoDto.getWeddingPlace().isEmpty()) {
                model.addAttribute("added_weddingInfo", registerWeddingInfoDto);
                model.addAttribute("added_operator", appUserService.findAll());
                model.addAttribute("error_message", "Need ceremony and wedding places");
                return "wedding/weddingInfoForm";
            }

            Optional<WeddingInfo> weddingInfoOptional = weddingInfoService.saveWeddingInfo(registerWeddingInfoDto);
            if (!weddingInfoOptional.isPresent()) {
                model.addAttribute("added_weddingInfo", registerWeddingInfoDto);
                model.addAttribute("added_operator", appUserService.findAll());
                model.addAttribute("error_message", "something went real bad :(");
                return "wedding/weddingInfoForm";
            }
            if (registerWeddingInfoDto.getModifiedId() == null) {
                return "redirect:/addPostProduction/" + clientInfoOptional.get().getId();
            } else {
                return "redirect:/modifyPostProduction/" + clientInfoOptional.get().getId();
            }
        }

        return "redirect:/";
    }


    @GetMapping("/modifyWeddingInfo/{id}")
    public String modify(Model model, @PathVariable(name = "id", required = true) Long id) {
        Optional<ClientInfo> clientInfoOptional = clientInfoService.findById(id);
        if (clientInfoOptional.isPresent()) {
            Optional<WeddingInfo> weddingInfoOptional = weddingInfoService.findByClientInfoId(id);
            WeddingInfo weddingInfo = weddingInfoOptional.get();

            RegisterWeddingInfoDto dto = RegisterWeddingInfoDto.createFrom(weddingInfo);
            dto.setModifiedId(id);
            model.addAttribute("added_weddingInfo", dto );
            model.addAttribute("added_operator", appUserService.findAll());

            return "wedding/weddingInfoForm";
        }

        return "redirect:/";


    }
}

