package com.javagda14.weddings.service;

import com.javagda14.weddings.model.Offer;
import com.javagda14.weddings.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OfferService {
    @Autowired
    private OfferRepository offerRepository;
    public Optional<Offer> findById(Long id) {
        return offerRepository.findById(id);
    }
    public List<Offer> findAll(){return offerRepository.findAll();}
}
