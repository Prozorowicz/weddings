package com.javagda14.weddings.service;

import com.javagda14.weddings.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class LoginService implements UserDetailsService {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<AppUser> appUserOptional = appUserService.findByUsername(username);
        if (appUserOptional.isPresent()) {
            // poprawne logowanie
            AppUser appUser = appUserOptional.get();

            return User
                    .withUsername(appUser.getUsername())
                    .password(appUser.getPassword())
                    .roles(extractRoles(appUser))
                    .build();
        }
        //
        throw new UsernameNotFoundException("User not found by name: " + username);
    }
    private String[] extractRoles(AppUser appUser) {
        List<String> roles = appUser.getRoles().stream().map(role -> role.getName().replace("ROLE_", "")).collect(Collectors.toList());
        String[] rolesArray = new String[roles.size()];
        rolesArray = roles.toArray(rolesArray);

        return rolesArray;
    }
    public Optional<AppUser> getLoggedInUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            // nie jesteśmy zalogowani
            return Optional.empty();
        }

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return appUserService.findByUsername(user.getUsername());
            // jesteśmy zalogowani, zwracamy user'a
        }

        return Optional.empty();
    }
}
