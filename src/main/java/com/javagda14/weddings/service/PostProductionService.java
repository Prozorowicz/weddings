package com.javagda14.weddings.service;

import com.javagda14.weddings.model.ClientInfo;
import com.javagda14.weddings.model.PostProduction;
import com.javagda14.weddings.model.dto.RegisterPostProductionDto;
import com.javagda14.weddings.repository.ClientInfoRepository;
import com.javagda14.weddings.repository.PostProductionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostProductionService {
    @Autowired
    private PostProductionRepository postProductionRepository;
    @Autowired
    private ClientInfoRepository clientInfoRepository;

    public Optional<PostProduction> register(RegisterPostProductionDto registerPostProductionDto) {


        Optional<ClientInfo> optionalClientInfo = clientInfoRepository.findById(registerPostProductionDto.getClientInfoId());
        if (optionalClientInfo.isPresent() ) {


            PostProduction postProduction = new PostProduction(null,
                   registerPostProductionDto.getRemainingPayment(),
                    registerPostProductionDto.getIsPaid(),
                    registerPostProductionDto.getTransportCosts(),
                    registerPostProductionDto.getFirstOperatorPaid(),
                    registerPostProductionDto.getSecondOperatorPaid(),
                    registerPostProductionDto.getClientNotes(),
                    registerPostProductionDto.getEditorsResponse(),
                    registerPostProductionDto.getLongFilmLink(),
                    registerPostProductionDto.getShortFilmLink(),
                    registerPostProductionDto.getAccepted(),
                    registerPostProductionDto.getPendriveSent(),
                    optionalClientInfo.get()
            );

            PostProduction newPostProduction = postProductionRepository.save(postProduction);
            ClientInfo clientInfo = optionalClientInfo.get();
            clientInfo.setPostProduction(newPostProduction);
            clientInfo = clientInfoRepository.save(clientInfo);
            return Optional.of(newPostProduction);
        }

        return Optional.empty();
    }
    public Optional<PostProduction> findByClientInfoId(Long id) {return postProductionRepository.findByClientInfo_Id(id);

    }

    public Optional<PostProduction> modify( RegisterPostProductionDto registerPostProductionDto) {
        Optional<PostProduction> postProductionOptional = findByClientInfoId(registerPostProductionDto.getClientInfoId());
        if (postProductionOptional.isPresent()) {
            PostProduction postProduction = postProductionOptional.get();
            postProduction.setRemainingPayment(registerPostProductionDto.getRemainingPayment());
            postProduction.setIsPaid(registerPostProductionDto.getIsPaid());
            postProduction.setTransportCosts(registerPostProductionDto.getTransportCosts());
            postProduction.setFirstOperatorPaid(registerPostProductionDto.getFirstOperatorPaid());
            postProduction.setSecondOperatorPaid(registerPostProductionDto.getSecondOperatorPaid());
            postProduction.setClientNotes(registerPostProductionDto.getClientNotes());
            postProduction.setEditorsResponse(registerPostProductionDto.getEditorsResponse());
            postProduction.setLongFilmLink(registerPostProductionDto.getLongFilmLink());
            postProduction.setShortFilmLink(registerPostProductionDto.getShortFilmLink());
            postProduction.setAccepted(registerPostProductionDto.getAccepted());
            postProduction.setPendriveSent(registerPostProductionDto.getPendriveSent());
            postProduction = postProductionRepository.save(postProduction);
            return Optional.of(postProduction);
        }
        return Optional.empty();
    }
}
