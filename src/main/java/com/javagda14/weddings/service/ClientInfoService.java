package com.javagda14.weddings.service;

import com.javagda14.weddings.model.ClientInfo;
import com.javagda14.weddings.model.Offer;
import com.javagda14.weddings.model.WeddingInfo;
import com.javagda14.weddings.model.dto.RegisterClientInfoDto;
import com.javagda14.weddings.repository.ClientInfoRepository;
import com.javagda14.weddings.repository.OfferRepository;
import com.javagda14.weddings.repository.WeddingInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class ClientInfoService {
    @Autowired
    private ClientInfoRepository clientInfoRepository;
    @Autowired
    private OfferRepository offerRepository;

    public Optional<ClientInfo> findById(Long id) {
        return clientInfoRepository.findById(id);
    }

    public List<ClientInfo> getAll() {
        return clientInfoRepository.findAll();
    }

    public Optional<ClientInfo> register(RegisterClientInfoDto registerClientInfoDto) {
        Optional<ClientInfo> optionalClientInfo = clientInfoRepository.findByEmail(registerClientInfoDto.getEmail());
        Optional<Offer> optionalOffer = offerRepository.findById(registerClientInfoDto.getOfferID());


        if (!optionalClientInfo.isPresent()) {

            ClientInfo clientInfo = new ClientInfo(null,
                    LocalDate.parse(registerClientInfoDto.getWeddingDate()),
                    registerClientInfoDto.getBrideName(),
                    registerClientInfoDto.getBrideSurname(),
                    registerClientInfoDto.getGroomName(),
                    registerClientInfoDto.getGroomSurname(),
                    registerClientInfoDto.getIdNumber(),
                    registerClientInfoDto.getAddress(),
                    registerClientInfoDto.getBridePhone(),
                    registerClientInfoDto.getGroomPhone(),
                    registerClientInfoDto.getEmail(),

                    registerClientInfoDto.getContract(),
                    registerClientInfoDto.getAdvertising(),
                    registerClientInfoDto.getAdvance(),
                    registerClientInfoDto.getDrone(),
                    optionalOffer.get(),
                    null,
                    null

            );

            ClientInfo newClientInfo = clientInfoRepository.save(clientInfo);

            return Optional.of(newClientInfo);
        }
        return Optional.empty();
    }


    public Optional<ClientInfo> modify(Long id, RegisterClientInfoDto registerClientInfoDto) {
        Optional<ClientInfo> clientInfoOptional = findById(id);
        if (clientInfoOptional.isPresent()) {
            ClientInfo clientInfo = clientInfoOptional.get();
            clientInfo.setWeddingDate(LocalDate.parse(registerClientInfoDto.getWeddingDate()));
            clientInfo.setBrideName(registerClientInfoDto.getBrideName());
            clientInfo.setBrideSurname(registerClientInfoDto.getBrideSurname());
            clientInfo.setGroomName(registerClientInfoDto.getGroomName());
            clientInfo.setGroomSurname(registerClientInfoDto.getGroomSurname());
            clientInfo.setIdNumber(registerClientInfoDto.getIdNumber());
            clientInfo.setAddress(registerClientInfoDto.getAddress());
            clientInfo.setBridePhone(registerClientInfoDto.getBridePhone());
            clientInfo.setGroomPhone(registerClientInfoDto.getGroomPhone());
            clientInfo.setEmail(registerClientInfoDto.getEmail());
            clientInfo.setContract(registerClientInfoDto.getContract());
            clientInfo.setAdvertising(registerClientInfoDto.getAdvertising());
            clientInfo.setAdvance(registerClientInfoDto.getAdvance());
            clientInfo.setDrone(registerClientInfoDto.getDrone());
            clientInfo.setOffer(offerRepository.findById(registerClientInfoDto.getOfferID()).get());
            clientInfo = clientInfoRepository.save(clientInfo);
            return Optional.of(clientInfo);
        }
        return Optional.empty();
    }

    public boolean remove(Long id) {
        Optional<ClientInfo> clientInfoOptional = clientInfoRepository.findById(id);
        if (clientInfoOptional.isPresent()) {
            clientInfoRepository.deleteById(id);
            return true;
        }
        return false;
    }
}