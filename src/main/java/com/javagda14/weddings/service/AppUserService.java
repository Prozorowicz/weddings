package com.javagda14.weddings.service;

import com.javagda14.weddings.model.AppUser;
import com.javagda14.weddings.model.UserRole;
import com.javagda14.weddings.model.dto.RegisterUserDto;
import com.javagda14.weddings.repository.AppUserRepository;
import com.javagda14.weddings.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AppUserService {
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private UserRoleRepository userRoleRepository;

    public Optional<AppUser> register(RegisterUserDto dto) {
        Optional<AppUser> optionalAppUser = appUserRepository.findByUsername(dto.getUsername());
        if (!optionalAppUser.isPresent()) {

            AppUser appUser = new AppUser(null,
                    dto.getEmail(),
                    dto.getUsername(),
                    dto.getName(),
                    dto.getSurname(),
                    encoder.encode(dto.getPassword()),
                    new HashSet<>(Arrays.asList(userRoleRepository.findByName("ROLE_OPERATOR").get()))

            );

            AppUser newAppUser = appUserRepository.save(appUser);

            return Optional.of(newAppUser);
        }
        return Optional.empty();
    }

    public Optional<AppUser> findByEmail(String email) {
        return appUserRepository.findByEmail(email);
    }

    public Optional<AppUser> findByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    public List<AppUser> findAll() {
        return appUserRepository.findAll();
    }
}
