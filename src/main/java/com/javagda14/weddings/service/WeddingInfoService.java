package com.javagda14.weddings.service;

import com.javagda14.weddings.model.AppUser;
import com.javagda14.weddings.model.ClientInfo;
import com.javagda14.weddings.model.WeddingInfo;
import com.javagda14.weddings.model.dto.RegisterWeddingInfoDto;
import com.javagda14.weddings.repository.AppUserRepository;
import com.javagda14.weddings.repository.ClientInfoRepository;
import com.javagda14.weddings.repository.WeddingInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WeddingInfoService {
    @Autowired
    private WeddingInfoRepository weddingInfoRepository;
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private ClientInfoRepository clientInfoRepository;

    public Optional<WeddingInfo> saveWeddingInfo(RegisterWeddingInfoDto registerWeddingInfoDto) {
        Optional<WeddingInfo> optionalWeddingInfo = weddingInfoRepository.findByClientInfo_Id(registerWeddingInfoDto.getClientInfoId());
        Optional<AppUser> optionalFirstOperator = appUserRepository.findById(registerWeddingInfoDto.getFirstOperatorID());
        Optional<AppUser> optionalSecondOperator = appUserRepository.findById(registerWeddingInfoDto.getSecondOperatorID());

        Optional<ClientInfo> optionalClientInfo = clientInfoRepository.findById(registerWeddingInfoDto.getClientInfoId());

        if(registerWeddingInfoDto.getModifiedId() == null) {
            if (optionalClientInfo.isPresent() && !optionalWeddingInfo.isPresent()) {
                WeddingInfo weddingInfo = new WeddingInfo(null,
                        registerWeddingInfoDto.getPrice(),
                        registerWeddingInfoDto.getBridePrep(),
                        registerWeddingInfoDto.getGroomPrep(),
                        registerWeddingInfoDto.getCeremonyPlace(),
                        registerWeddingInfoDto.getWeddingPlace(),
                        registerWeddingInfoDto.getDistance(),
                        registerWeddingInfoDto.getDistancePayment(),
                        optionalFirstOperator.get(),
                        optionalSecondOperator.get(),
                        optionalClientInfo.get()
                );


                WeddingInfo newWeddingInfo = weddingInfoRepository.save(weddingInfo);
                ClientInfo clientInfo = optionalClientInfo.get();
                clientInfo.setWeddingInfo(newWeddingInfo);
                clientInfo = clientInfoRepository.save(clientInfo);
                return Optional.of(newWeddingInfo);
            }
        }else if(optionalClientInfo.isPresent()){
            return modify(registerWeddingInfoDto.getClientInfoId(), registerWeddingInfoDto);
        }

        return Optional.empty();
    }

    public Optional<WeddingInfo> findByClientInfoId(Long id) {
        return weddingInfoRepository.findByClientInfo_Id(id);
    }

    public Optional<WeddingInfo> modify(Long id, RegisterWeddingInfoDto registerWeddingInfoDto) {
        Optional<WeddingInfo> weddingInfoOptional = findByClientInfoId(id);
        if (weddingInfoOptional.isPresent()) {
            WeddingInfo weddingInfo = weddingInfoOptional.get();
            weddingInfo.setPrice(registerWeddingInfoDto.getPrice());
            weddingInfo.setBridePrep(registerWeddingInfoDto.getBridePrep());
            weddingInfo.setGroomPrep(registerWeddingInfoDto.getGroomPrep());
            weddingInfo.setCeremonyPlace(registerWeddingInfoDto.getCeremonyPlace());
            weddingInfo.setWeddingPlace(registerWeddingInfoDto.getWeddingPlace());
            weddingInfo.setDistance(registerWeddingInfoDto.getDistance());
            weddingInfo.setDistancePayment(registerWeddingInfoDto.getDistancePayment());
            weddingInfo.setFirstOperator(appUserRepository.findById(registerWeddingInfoDto.getFirstOperatorID()).get());
            weddingInfo.setSecondOperator(appUserRepository.findById(registerWeddingInfoDto.getSecondOperatorID()).get());

            weddingInfo = weddingInfoRepository.save(weddingInfo);
            return Optional.of(weddingInfo);
        }
        return Optional.empty();
    }

}