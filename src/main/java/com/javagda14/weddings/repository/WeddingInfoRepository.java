package com.javagda14.weddings.repository;

import com.javagda14.weddings.model.WeddingInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WeddingInfoRepository extends JpaRepository<WeddingInfo, Long> {
    Optional<WeddingInfo>findByBridePrep(String bridePrep);

    Optional<WeddingInfo> findByClientInfo_Id(Long id);

}
