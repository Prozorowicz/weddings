package com.javagda14.weddings.repository;

import com.javagda14.weddings.model.PostProduction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostProductionRepository extends JpaRepository<PostProduction, Long> {
    Optional<PostProduction> findByClientInfo_Id(Long id);
}
