package com.javagda14.weddings.component;

import com.javagda14.weddings.model.AppUser;
import com.javagda14.weddings.model.Offer;
import com.javagda14.weddings.model.UserRole;
import com.javagda14.weddings.repository.AppUserRepository;
import com.javagda14.weddings.repository.OfferRepository;
import com.javagda14.weddings.repository.UserRoleRepository;
import com.javagda14.weddings.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Component
public class InitialDataInitializer implements
        ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        addRole("ROLE_ADMIN");
        addRole("ROLE_EDITOR");
        addRole("ROLE_OPERATOR");

        addUser("admin", "admin", "ROLE_ADMIN", "ROLE_EDITOR", "ROLE_OPERATOR");
        addUser("editor", "editor", "ROLE_EDITOR", "ROLE_OPERATOR");
        addUser("operator", "operator", "ROLE_OPERATOR");

        addOffer("PREMIUM", 7900);
        addOffer("GOLD", 3900);
        addOffer("SILVER", 2900);
        addOffer("OTHER", 0);
    }

    @Transactional
    public void addRole(String name) {
        Optional<UserRole> userRoleOptional = userRoleRepository.findByName(name);
        if (!userRoleOptional.isPresent()) {
            userRoleRepository.save(new UserRole(null, name));
        }
    }

    @Transactional
    public void addOffer(String name, Integer price) {
        Optional<Offer> optionalOffer = offerRepository.findByName(name);
        if (!optionalOffer.isPresent()) {
            offerRepository.save(new Offer(null, name, price));
        }

    }

    @Transactional
    public void addUser(String username, String password, String... roles) {
        Optional<AppUser> appUserOptional = appUserRepository.findByUsername(username);
        if (!appUserOptional.isPresent()) {
            List<UserRole> rolesList = new ArrayList<>();

            for (String role : roles) {
                Optional<UserRole> userRoleOptional = userRoleRepository.findByName(role);
                userRoleOptional.ifPresent(rolesList::add);
            }

            appUserRepository.save(new AppUser(
                    null,
                    null,
                    username,
                    null,
                    null,
                    bCryptPasswordEncoder.encode(password),
                    new HashSet<UserRole>(rolesList)));

        }
    }
}

