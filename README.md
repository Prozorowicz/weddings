# Weddings
## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is a client database managment portal for a wedding videography company.  
Company had several film crews, editor and a customer service person.  
There was a problem with information flow.  
This project was desinged as company's internal site for gathering info from clients and easy access to it for every employee.  
Three different types of users could be created each with different levels of access.
	
## Technologies
Project is created with:

* Java version: 8

* Maven

* Spring Boot 2.0.6 with:

  * Web
  
  * Data JPA
  
  * Lombok
  
  * Bootstrap
  
  * Jquery
	
## Setup
Work in progress.